﻿namespace DeCock_Nicolas_SE
{
    partial class GameDemoUI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitel = new System.Windows.Forms.Label();
            this.lblUur = new System.Windows.Forms.Label();
            this.lblUurVar = new System.Windows.Forms.Label();
            this.lblPlaats = new System.Windows.Forms.Label();
            this.lblPlaatsVar = new System.Windows.Forms.Label();
            this.lblDuur = new System.Windows.Forms.Label();
            this.lblDuurVar = new System.Windows.Forms.Label();
            this.lblBeschrijving = new System.Windows.Forms.Label();
            this.txtBoxBeschrijving = new System.Windows.Forms.TextBox();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.listBoxStudent = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lblTitel
            // 
            this.lblTitel.AutoSize = true;
            this.lblTitel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTitel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitel.ForeColor = System.Drawing.Color.MediumSlateBlue;
            this.lblTitel.Location = new System.Drawing.Point(3, 9);
            this.lblTitel.Name = "lblTitel";
            this.lblTitel.Size = new System.Drawing.Size(142, 29);
            this.lblTitel.TabIndex = 0;
            this.lblTitel.Text = "Titel Demo";
            this.lblTitel.Click += new System.EventHandler(this.lblTitel_Click);
            // 
            // lblUur
            // 
            this.lblUur.AutoSize = true;
            this.lblUur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUur.Location = new System.Drawing.Point(3, 52);
            this.lblUur.Name = "lblUur";
            this.lblUur.Size = new System.Drawing.Size(39, 20);
            this.lblUur.TabIndex = 1;
            this.lblUur.Text = "Uur:";
            // 
            // lblUurVar
            // 
            this.lblUurVar.AutoSize = true;
            this.lblUurVar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUurVar.Location = new System.Drawing.Point(38, 52);
            this.lblUurVar.Name = "lblUurVar";
            this.lblUurVar.Size = new System.Drawing.Size(36, 20);
            this.lblUurVar.TabIndex = 2;
            this.lblUurVar.Text = "14u";
            // 
            // lblPlaats
            // 
            this.lblPlaats.AutoSize = true;
            this.lblPlaats.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlaats.Location = new System.Drawing.Point(3, 72);
            this.lblPlaats.Name = "lblPlaats";
            this.lblPlaats.Size = new System.Drawing.Size(57, 20);
            this.lblPlaats.TabIndex = 3;
            this.lblPlaats.Text = "Plaats:";
            // 
            // lblPlaatsVar
            // 
            this.lblPlaatsVar.AutoSize = true;
            this.lblPlaatsVar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlaatsVar.Location = new System.Drawing.Point(57, 72);
            this.lblPlaatsVar.Name = "lblPlaatsVar";
            this.lblPlaatsVar.Size = new System.Drawing.Size(47, 20);
            this.lblPlaatsVar.TabIndex = 4;
            this.lblPlaatsVar.Text = "C104";
            // 
            // lblDuur
            // 
            this.lblDuur.AutoSize = true;
            this.lblDuur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDuur.Location = new System.Drawing.Point(110, 52);
            this.lblDuur.Name = "lblDuur";
            this.lblDuur.Size = new System.Drawing.Size(48, 20);
            this.lblDuur.TabIndex = 5;
            this.lblDuur.Text = "Duur:";
            // 
            // lblDuurVar
            // 
            this.lblDuurVar.AutoSize = true;
            this.lblDuurVar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDuurVar.Location = new System.Drawing.Point(164, 52);
            this.lblDuurVar.Name = "lblDuurVar";
            this.lblDuurVar.Size = new System.Drawing.Size(27, 20);
            this.lblDuurVar.TabIndex = 6;
            this.lblDuurVar.Text = "2u";
            // 
            // lblBeschrijving
            // 
            this.lblBeschrijving.AutoSize = true;
            this.lblBeschrijving.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBeschrijving.Location = new System.Drawing.Point(3, 106);
            this.lblBeschrijving.Name = "lblBeschrijving";
            this.lblBeschrijving.Size = new System.Drawing.Size(101, 20);
            this.lblBeschrijving.TabIndex = 7;
            this.lblBeschrijving.Text = "Beschrijving: ";
            // 
            // txtBoxBeschrijving
            // 
            this.txtBoxBeschrijving.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBoxBeschrijving.Enabled = false;
            this.txtBoxBeschrijving.Location = new System.Drawing.Point(7, 129);
            this.txtBoxBeschrijving.Multiline = true;
            this.txtBoxBeschrijving.Name = "txtBoxBeschrijving";
            this.txtBoxBeschrijving.ReadOnly = true;
            this.txtBoxBeschrijving.Size = new System.Drawing.Size(210, 51);
            this.txtBoxBeschrijving.TabIndex = 8;
            // 
            // btnPrevious
            // 
            this.btnPrevious.Location = new System.Drawing.Point(0, 163);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(60, 23);
            this.btnPrevious.TabIndex = 10;
            this.btnPrevious.Text = "< Terug";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(161, 163);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(67, 23);
            this.btnConfirm.TabIndex = 11;
            this.btnConfirm.Text = "Stemmen";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // listBoxStudent
            // 
            this.listBoxStudent.FormattingEnabled = true;
            this.listBoxStudent.Location = new System.Drawing.Point(-3, 0);
            this.listBoxStudent.Name = "listBoxStudent";
            this.listBoxStudent.Size = new System.Drawing.Size(231, 186);
            this.listBoxStudent.TabIndex = 12;
            // 
            // GameDemoUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.listBoxStudent);
            this.Controls.Add(this.txtBoxBeschrijving);
            this.Controls.Add(this.lblBeschrijving);
            this.Controls.Add(this.lblDuurVar);
            this.Controls.Add(this.lblDuur);
            this.Controls.Add(this.lblPlaatsVar);
            this.Controls.Add(this.lblPlaats);
            this.Controls.Add(this.lblUur);
            this.Controls.Add(this.lblUurVar);
            this.Controls.Add(this.lblTitel);
            this.Name = "GameDemoUI";
            this.Size = new System.Drawing.Size(231, 200);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitel;
        private System.Windows.Forms.Label lblUur;
        private System.Windows.Forms.Label lblUurVar;
        private System.Windows.Forms.Label lblPlaats;
        private System.Windows.Forms.Label lblPlaatsVar;
        private System.Windows.Forms.Label lblDuur;
        private System.Windows.Forms.Label lblDuurVar;
        private System.Windows.Forms.Label lblBeschrijving;
        private System.Windows.Forms.TextBox txtBoxBeschrijving;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.ListBox listBoxStudent;
    }
}
