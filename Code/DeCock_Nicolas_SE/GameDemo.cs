﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeCock_Nicolas_SE
{
    public class GameDemo
    {
        private string mPlace;
        private int mTime;
        private int mDuration;
        private string mTitle;
        private string mDescription;

        GameDemoUI mUI;
        List<GameDemoUI> observers = new List<GameDemoUI>();

        public string Title
        {
            get { return mTitle; }
            set { mTitle = value; }
        }

        public string Place
        {
            get { return mPlace; }
        }

        public int Time
        {
            get { return mTime; }
        }

        public int Duration
        {
            get { return mDuration; }
        }

        public string Description
        {
            get { return mDescription; }
        }

        public GameDemo(string initTitle,string initPlace,int initTime,int initDuration,string initDescription)
        {
            mTitle = initTitle;
            mPlace = initPlace;
            mTime = initTime;
            mDuration = initDuration;
            mDescription = initDescription;
        }

        public void addUI(GameDemoUI ui)
        {
            mUI = ui;
        }

        public void leegMakenList()
        {
            mUI.maakListLeeg();
        }

        public void addStudent(string stud)
        {
            mUI.addStudentenList(stud);
        }

        public void gestemd()
        {
            mUI.minEenStem();
            //notifyObservers();
        }

        /*public void addObserver(GameDemoUI observ)
        {
            observers.Add(observ);
        }

        private void notifyObservers()
        {
            foreach (GameDemoUI e in observers)
            {
                //update iets (bv aantal stemmen nog over)
            }
        }*/
    }
}
