﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeCock_Nicolas_SE
{
    public partial class GameDemoUI : UserControl
    {

        private GameDemo model;
        private GameDemoController controller;

        private static int stemmen = 3;

        public GameDemoUI(GameDemo demo,GameDemoController c)
        {
            InitializeComponent();
            model = demo;
            controller = c;
            updateUI();
        }

        private void lblTitel_Click(object sender, EventArgs e)
        {
            controller.demoGeklikt(model.Title);
            updateUI();
        }

        public void updateUI()
        {
            btnPrevious.Visible =! btnPrevious.Visible;
            btnConfirm.Visible = !btnConfirm.Visible;
            listBoxStudent.Visible =! listBoxStudent.Visible;
            
            lblTitel.Text = model.Title;
            lblPlaatsVar.Text = model.Place;
            lblUurVar.Text = model.Time.ToString() + "u";
            lblDuurVar.Text = model.Duration.ToString() + "u";
            txtBoxBeschrijving.Text = model.Description;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            updateUI();          
        }

        public void addStudentenList(string studInfo)
        {
            listBoxStudent.Items.Add(studInfo);        
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                if (stemmen > 0)
                {
                    controller.btnStemmenGeklikt(listBoxStudent.SelectedItem.ToString());
                    if (stemmen == 0)
                    {
                        MessageBox.Show("Geen stemmen meer over!!");
                    }
                    else
                    {
                        MessageBox.Show("U hebt nog " + stemmen + " stem(men) over");
                    }                    
                    updateUI();
                }
                else
                {
                    MessageBox.Show("U hebt geen stemmen meer!!");
                }
            }
            catch
            {
                MessageBox.Show("U hebt nog geen student geselecteerd!!");
            }
            finally
            {                
                
            }
        }

        public void maakListLeeg()
        {
            listBoxStudent.Items.Clear();
        }

        public void minEenStem()
        {
            stemmen--;
        }
    }
}
