﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeCock_Nicolas_SE
{
    public class Student
    {
        private string mVoornaam;
        private string mNaam;
        
        private string mDemo;
        private int stemmen;

        public string getNaam
        {
            get { return mNaam; }
        }

        public string getVoornaam
        {
            get { return mVoornaam; }
        }

        public string getDemo
        {
            get { return mDemo; }
        }

        public int getAantalStemmen
        {
            get { return stemmen; }
        }

        public Student(string initNaam,string initVoornaam,string initDemo)
        {
            mNaam = initNaam;
            mVoornaam = initVoornaam;
            mDemo = initDemo;
            stemmen = 0;
        }

        public void gestemd()
        {
            stemmen++;
        }
    }
}
