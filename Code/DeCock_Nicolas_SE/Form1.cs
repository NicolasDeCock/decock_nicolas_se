﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeCock_Nicolas_SE
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

             GameDemo[] arrDemos = new GameDemo[6];
             arrDemos[0] = new GameDemo("Assasins Demo","B212",13,3,"een derdepersoons actie avontuur spel over een assasin");
             arrDemos[1] = new GameDemo("Calladuty Demo", "C002", 14, 1, "Een eerste persoons schietergame over terrorisme en 'merica");
             arrDemos[2] = new GameDemo("Racing Demo", "C204", 15, 2, "Realistisch racen met de beste graphics");
             arrDemos[3] = new GameDemo("Red alert 5 Demo", "C002", 15, 1, "Strategie van oud tot nieuw met uitgebreide gameplay");
             arrDemos[4] = new GameDemo("LoL Demo", "B212", 16, 4, "'S werelds meest gespeeld MOBA (multiplayer online battle arena)");
             arrDemos[5] = new GameDemo("GTA Demo", "C204", 17, 2, "De wereldbekende 3th person actie avontuur game");

             GameDemoController[] controllers = new GameDemoController[6];

             for (int i = 0; i < arrDemos.Length; i++)
             {
                 controllers[i] = new GameDemoController(arrDemos[i]);
             }

             GameDemoUI view = controllers[0].getDemoUI(arrDemos[0]);
             view.Location   = new System.Drawing.Point(0,30);
             view.Size       = new System.Drawing.Size(250, 200);
             this.Controls.Add(view);

             GameDemoUI view2 = controllers[1].getDemoUI(arrDemos[1]);
             view2.Location = new System.Drawing.Point(250,30);
             view2.Size = new System.Drawing.Size(250, 200);
             this.Controls.Add(view2);

             GameDemoUI view3 = controllers[2].getDemoUI(arrDemos[2]);
             view3.Location = new System.Drawing.Point(0, 230);
             view3.Size = new System.Drawing.Size(250, 200);
             this.Controls.Add(view3);

             GameDemoUI view4 = controllers[3].getDemoUI(arrDemos[3]);
             view4.Location = new System.Drawing.Point(250, 230);
             view4.Size = new System.Drawing.Size(250, 200);
             this.Controls.Add(view4);

             GameDemoUI view5 = controllers[4].getDemoUI(arrDemos[4]);
             view5.Location = new System.Drawing.Point(0, 430);
             view5.Size = new System.Drawing.Size(250, 200);
             this.Controls.Add(view5);

             GameDemoUI view6 = controllers[5].getDemoUI(arrDemos[5]);
             view6.Location = new System.Drawing.Point(250, 430);
             view6.Size = new System.Drawing.Size(308, 279);
             this.Controls.Add(view6);
        }
    }
}
