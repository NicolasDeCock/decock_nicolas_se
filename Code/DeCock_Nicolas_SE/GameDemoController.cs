﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeCock_Nicolas_SE
{
    public class GameDemoController
    {
        private GameDemo mDemo;
        private List<Student> mStudenten = new List<Student>();


        public GameDemoController(GameDemo d)
        {
            mDemo = d;
            mStudenten.Add(new Student("Leurs", "Filip", "Assasins Demo"));
            mStudenten.Add(new Student("De Cock", "Nicolas", "Assasins Demo"));
            mStudenten.Add(new Student("Wauters", "Laurens", "Assasins Demo"));
            mStudenten.Add(new Student("De Meyer", "Mats", "Calladuty Demo"));
            mStudenten.Add(new Student("De Groote", "Simon", "Calladuty Demo"));
            mStudenten.Add(new Student("Debode", "Quentin", "Racing Demo"));
            mStudenten.Add(new Student("De Cuyper", "Arnd", "Racing Demo"));
            mStudenten.Add(new Student("Van Looveren", "Charis", "Racing Demo"));
            mStudenten.Add(new Student("Jacobs", "Beau", "Red alert 5 Demo"));
            mStudenten.Add(new Student("Boons", "Jens", "LoL Demo"));
            mStudenten.Add(new Student("Van Praag", "Michel", "LoL Demo"));
            mStudenten.Add(new Student("Goetschalckx", "Andries", "LoL Demo"));
            mStudenten.Add(new Student("De Boeck", "Tim", "GTA Demo"));
            mStudenten.Add(new Student("Matthijsen", "Stijn", "GTA Demo"));
        }

        public void demoGeklikt(string titelDemo)
        {
            mDemo.leegMakenList();

            for (int i = 0; i < 14; i++)
            {
                if (mStudenten[i].getDemo == titelDemo)
                {
                    mDemo.addStudent(mStudenten[i].getVoornaam + " " + mStudenten[i].getNaam + "  stemmen= " + mStudenten[i].getAantalStemmen);
                }
            }
        }

        public void btnStemmenGeklikt(string stem)
        {
            for (int i = 0; i < 14; i++)
            {
                if (stem == mStudenten[i].getVoornaam + " " + mStudenten[i].getNaam + "  stemmen= " + mStudenten[i].getAantalStemmen)
                {
                    mStudenten[i].gestemd();
                    mDemo.gestemd();
                }
            }
        }

        public GameDemoUI getDemoUI(GameDemo d)
        {
            GameDemoUI view = new GameDemoUI(d, this);
            mDemo.addUI(view);
            //mDemo.addObserver(view);
            return view;
        }
    }
}
