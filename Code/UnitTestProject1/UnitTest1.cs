﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DeCock_Nicolas_SE;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {

        private GameDemo demo;
        private GameDemoController control;
        private GameDemoUI ui;
        

        [TestInitialize]
        public void initialiseerTeerling()
        {
            demo = new GameDemo("titel","plaats",5,2,"beschrijving");
            control = new GameDemoController(demo);
            ui = new GameDemoUI(demo, control);
        }

        [TestMethod]
        public void TestMethod1()
        {
            demo.gestemd();
            Assert.IsTrue(2 * 5 == 10);
            Assert.IsTrue(ui.Stemmen == 2);
            
        }
    }
}
