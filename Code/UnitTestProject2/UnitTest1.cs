﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DeCock_Nicolas_SE;

namespace UnitTestProject2
{
    [TestClass]
    public class UnitTest1
    {
        private Student stud;

        [TestInitialize]
        public void initialiseerTest()
        {
            stud = new Student("decock", "nico", "AC demo");
        }

        [TestMethod]
        public void TestStemmenStudent()
        {
            for (int i = 0; i <= 1000; i++)
            {
                stud.gestemd();
            }
            Assert.IsTrue(stud.getAantalStemmen > 1000);
        }
    }
}
